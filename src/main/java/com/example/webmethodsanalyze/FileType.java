package com.example.webmethodsanalyze;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

public class FileType {
    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());
        static boolean Error = false;


        public static void verifyFlowStructure(PackageNode node) {
            try {
                if ((node.getxmlContent() != null) && (node.getName().endsWith(".xml")) && (node.getParent() != null) && (node.getParent().getName().equals("FLOW"))) {
                    Node rootNode = node.getxmlContent();

                    if (containsSequenceElement(rootNode)) {
                        if (!verifySequenceStructure(rootNode, false)) {

                            logger.severe("Invalid sequence structure in " + node.getParent().getName());
                            if (!Error) {
                                Error = true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.severe("Exception in verifyFlowStructure: " + e.getMessage());
                if (!Error) {
                    Error = true;
                }
                e.printStackTrace();
            }
        }



    public static boolean containsSequenceElement(Node node) {
        if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
            if ("SEQUENCE".equals(node.getNodeName())) {
                return true;
            }

            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                if (containsSequenceElement(children.item(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean verifySequenceStructure(Node node, boolean isWithinBranch) {
        if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
            if ("SEQUENCE".equals(node.getNodeName())) {
                Element element = (Element) node;
                if (!isWithinBranch && "SUCCESS".equals(element.getAttribute("EXIT-ON"))) {
                    NodeList childNodes = element.getChildNodes();
                    boolean hasTryFailure = false;
                    boolean hasCatchDone = false;

                    for (int i = 0; i < childNodes.getLength(); i++) {
                        Node child = childNodes.item(i);
                        if (child.getNodeType() == Node.ELEMENT_NODE && "SEQUENCE".equals(child.getNodeName())) {
                            Element childElement = (Element) child;
                            if ("FAILURE".equals(childElement.getAttribute("EXIT-ON"))) {
                                hasTryFailure = true;
                            } else if ("DONE".equals(childElement.getAttribute("EXIT-ON"))) {
                                hasCatchDone = true;
                            }
                        }
                    }
                    return hasTryFailure && hasCatchDone;
                } else if (isWithinBranch) {
                    // If the sequence is within a branch, only check for proper EXIT-ON attributes
                    return "FAILURE".equals(element.getAttribute("EXIT-ON")) || "DONE".equals(element.getAttribute("EXIT-ON"));
                }
            } else if ("BRANCH".equals(node.getNodeName())) {
                NodeList children = node.getChildNodes();
                for (int i = 0; i < children.getLength(); i++) {
                    // Recursively check sequences within a branch
                    if (!verifySequenceStructure(children.item(i), true)) {
                        return false;
                    }
                }
                return true;
            }

            // Recursively check all child nodes
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                if (!verifySequenceStructure(children.item(i), isWithinBranch)) {
                    return false;
                }
            }
        }
        return true;
    }


}
