package com.example.webmethodsanalyze;

import java.util.logging.Logger;

public class PackageStructureLogger {

    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());
    static Boolean Error=false;
    public static void  logStructure(PackageNode node, String type) throws PackageStructureException {

        if(node.getType().toString().equals(type)){


        String validationMessage = NamingConventionVerifier.validateNodeName(node, node.getParent() != null ? node.getParent().getName() : node.getName());
        if (validationMessage.startsWith("Invalid")) {
            logger.severe( validationMessage);
            if(!Error){
                Error=true;
            }
        }else if (validationMessage.startsWith("Valid")){
            logger.info( validationMessage);
        }
        }
        for (PackageNode child : node.getChildren()) {
            logStructure(child, type);
        }
        if(Error){
            throw new PackageStructureException("Invalid Naming Convention check logs for more details");
        }

    }

    public static void  SequenceStructure(PackageNode node) throws PackageStructureException {
        FileType.verifyFlowStructure(node);

        for (PackageNode child : node.getChildren()) {
            SequenceStructure(child);
        }
        if(FileType.Error){
            throw new PackageStructureException("Invalid Sequence Structure check logs for more details");
        }
    }
}
